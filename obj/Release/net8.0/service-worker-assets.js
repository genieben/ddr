﻿self.assetsManifest = {
  "assets": [
    {
      "hash": "sha256-k8NfwuwCCeOoFCP+4FuP9FeUzGyohHbIWvOxkXEPg\/w=",
      "url": "_framework\/blazor.webassembly.js"
    },
    {
      "hash": "sha256-Em4vSDFByVyY4UCoBjdI3NJnfZfhp+hdb+DeXpdhKS8=",
      "url": "_framework\/dotnet.js"
    },
    {
      "hash": "sha256-gPMK16QWoJSTxorlZJqcVtbBRTniIQWlLsJN2O4cJHg=",
      "url": "_framework\/dotnet.native.8.0.2.8emy76ij2b.js"
    },
    {
      "hash": "sha256-C2P+OTYZAJlmqo6rc4heqwvIVVsXIF8MAxC6WRe96Eg=",
      "url": "_framework\/dotnet.native.wasm"
    },
    {
      "hash": "sha256-54KSaoOArNfygQbV+r4B5y3ys53v2tnvFIyZcZiUmi0=",
      "url": "_framework\/dotnet.runtime.8.0.2.wa2wucv3j5.js"
    },
    {
      "hash": "sha256-SZLtQnRc0JkwqHab0VUVP7T3uBPSeYzxzDnpxPpUnHk=",
      "url": "_framework\/icudt_CJK.dat"
    },
    {
      "hash": "sha256-8fItetYY8kQ0ww6oxwTLiT3oXlBwHKumbeP2pRF4yTc=",
      "url": "_framework\/icudt_EFIGS.dat"
    },
    {
      "hash": "sha256-L7sV7NEYP37\/Qr2FPCePo5cJqRgTXRwGHuwF5Q+0Nfs=",
      "url": "_framework\/icudt_no_CJK.dat"
    },
    {
      "hash": "sha256-RDvZOqtNjjGJo+OuGb4yNhPgVEJP\/jhUNOI+6Ed2ADU=",
      "url": "_framework\/blazor.boot.json"
    },
    {
      "hash": "sha256-Dyc7RWK2McGVUnEGKQWezf5SQhR8QlsVYP1L0SQ0txk=",
      "url": "_framework\/Microsoft.AspNetCore.Components.wasm"
    },
    {
      "hash": "sha256-0KisFlCQGsFQexOgjsJ2pZiu6mZAgVpvKoIM5ngHeD4=",
      "url": "_framework\/Microsoft.AspNetCore.Components.Web.wasm"
    },
    {
      "hash": "sha256-jrdFRjrF1131MZ3HGQVsI3dX8cLdZvPz85lsBltxjSM=",
      "url": "_framework\/Microsoft.AspNetCore.Components.WebAssembly.wasm"
    },
    {
      "hash": "sha256-87sn2TYqgdZ95sXmavjKEzoEfMgHnYQ9LOvnMX+aZcI=",
      "url": "_framework\/Microsoft.Extensions.Configuration.Abstractions.wasm"
    },
    {
      "hash": "sha256-Sxmy2ZS134URxbHEvdbS6NcQ3zXS7UWx\/5ZPpwiW7FA=",
      "url": "_framework\/Microsoft.Extensions.Configuration.Json.wasm"
    },
    {
      "hash": "sha256-jYqHUZ07UYWc8POk7xhas6xQYH7t1qoTcylqoDqncJk=",
      "url": "_framework\/Microsoft.Extensions.Configuration.wasm"
    },
    {
      "hash": "sha256-qFPcWaRT3TLje0Ka8NtmslI37vhqDxIH0UFb3lOPxVs=",
      "url": "_framework\/Microsoft.Extensions.DependencyInjection.Abstractions.wasm"
    },
    {
      "hash": "sha256-gg8xZqJsBBrrNEyUGzYqhL3sqkuZ4AHAvdTdL9nZ0S0=",
      "url": "_framework\/Microsoft.Extensions.DependencyInjection.wasm"
    },
    {
      "hash": "sha256-+Fgk2a7GOZ76M3YfJ+0IDUowOMx8\/LBnDwuIKQr+\/IA=",
      "url": "_framework\/Microsoft.Extensions.Logging.Abstractions.wasm"
    },
    {
      "hash": "sha256-8BH+kQfjYuZWxprOICXJ4+tU0OdJOYDKN7G0S3zYYHI=",
      "url": "_framework\/Microsoft.Extensions.Logging.wasm"
    },
    {
      "hash": "sha256-ezKJDNjeghS\/qMdQmUpBPdH7bCDLrqkNqS6RsLxlpOY=",
      "url": "_framework\/Microsoft.Extensions.Options.wasm"
    },
    {
      "hash": "sha256-6187ynEahDlSLMBvD4vAmiLpZ3clRb5xu6rM7O8AxNo=",
      "url": "_framework\/Microsoft.Extensions.Primitives.wasm"
    },
    {
      "hash": "sha256-1vXTFGB8f1xknhrY0hxfr04x1\/JK+bA0IxTp2CBzmhY=",
      "url": "_framework\/Microsoft.JSInterop.wasm"
    },
    {
      "hash": "sha256-2qBJkLnOCr4py2S0peps0cFo0z3Ew9OVwGYodtvAVkk=",
      "url": "_framework\/Microsoft.JSInterop.WebAssembly.wasm"
    },
    {
      "hash": "sha256-XbjsqBTchgqQCvntOGVltROaIBvjm3FJpSpBetcl4bs=",
      "url": "_framework\/ram-pn-decoder.wasm"
    },
    {
      "hash": "sha256-DEZ59fGbj2NqpZbDf9KV0hzXZkN0+vJaLNPPONQTjLo=",
      "url": "_framework\/System.Collections.Concurrent.wasm"
    },
    {
      "hash": "sha256-le3Hos20YHqMbju0NlRous5zMDdzqVrcRrbQ1gFpssU=",
      "url": "_framework\/System.Collections.wasm"
    },
    {
      "hash": "sha256-qwurUoDSjnjNxAivtUxZqGvkOgARzX9ETuhBPrMx5Xo=",
      "url": "_framework\/System.ComponentModel.wasm"
    },
    {
      "hash": "sha256-hcaSe3dddvUYzyPNpqoGVuiON\/PLZocvLhbJ0wtpre8=",
      "url": "_framework\/System.Console.wasm"
    },
    {
      "hash": "sha256-uOn2NQpv\/BE0J7ag0lhIKWsUcxg3U4uEeZeXx+eIKrk=",
      "url": "_framework\/System.Linq.wasm"
    },
    {
      "hash": "sha256-is0fLYQZE9huTBti4rGQQebqsfCdCmdu0IQTpRGE5VQ=",
      "url": "_framework\/System.Memory.wasm"
    },
    {
      "hash": "sha256-6m19mkuheow7Jv1+bmjfwJffNfxGHJrDu+eWAmXGKas=",
      "url": "_framework\/System.Net.Http.wasm"
    },
    {
      "hash": "sha256-sm6hDRoze7+24GQmGGL6eyXuz9nBcj3qAI8bOqlSVnc=",
      "url": "_framework\/System.Net.Primitives.wasm"
    },
    {
      "hash": "sha256-lveCPMBxTx5DG7VdG2HBbmgESzEdpPqEdWZjwAjjFnE=",
      "url": "_framework\/System.Private.CoreLib.wasm"
    },
    {
      "hash": "sha256-Tf5JCVMO0LUer4ja6V4cqDi+m4Uw5ieHq4JjdF5zJVI=",
      "url": "_framework\/System.Private.Uri.wasm"
    },
    {
      "hash": "sha256-V1xvZgzz6Gy7iYFPviaHRPOvygBTDwW\/AT5\/QUajEOM=",
      "url": "_framework\/System.Runtime.InteropServices.JavaScript.wasm"
    },
    {
      "hash": "sha256-k85maETULfXLf\/+GGLuI+W18HFvbvM9eFmcyz\/HER+k=",
      "url": "_framework\/System.Runtime.wasm"
    },
    {
      "hash": "sha256-NnePSWCgIId8nOPCgziTKhDyG6uCBs+3W6fazrWLoUA=",
      "url": "_framework\/System.Text.Encodings.Web.wasm"
    },
    {
      "hash": "sha256-GR3D89PGASVanrViLBaCIgEJEFS4z7o+W7m7kECsfC0=",
      "url": "_framework\/System.Text.Json.wasm"
    },
    {
      "hash": "sha256-W20q\/uEUAv0A+F6irUkY0pCZJo76wNuWpz3XgcLFB2M=",
      "url": "_framework\/System.Text.RegularExpressions.wasm"
    },
    {
      "hash": "sha256-8LTSrmUHiaqHDH\/8\/xxyajqRoJxNpbxBcdbxcRB+t60=",
      "url": "css\/app.css"
    },
    {
      "hash": "sha256-DbpQaq68ZSb5IoPosBErM1QWBfsbTxpJqhU0REi6wP4=",
      "url": "icon-192.png"
    },
    {
      "hash": "sha256-oEo6d+KqX5fjxTiZk\/w9NB3Mi0+ycS5yLwCKwr4IkbA=",
      "url": "icon-512.png"
    },
    {
      "hash": "sha256-z\/kRBnK06S4VcunBrRUeZmPq8WdV7onhjbIBFdZgzyQ=",
      "url": "index.html"
    },
    {
      "hash": "sha256-Dcs0NcRUBKeFOh+Wxfvd5esDD1L3eJCNq\/34PcTsWDM=",
      "url": "manifest.webmanifest"
    }
  ],
  "version": "ry3bZpZM"
};
